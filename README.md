# Welcome to the RNABC repository!

The RNABC pipeline is used for data processing in �Using microarray-based subtyping methods for breast cancer in the era of high-throughput RNA sequencing� by Pedersen C. B., Nielsen, F. C., Rossing, M., and Olsen, L. R., 2018.
The code was written by [Christina Bligaard Pedersen](https://www.dtu.dk/english/service/phonebook/person?id=88697&cpid=176923) and [Lars R�nn Olsen](https://www.dtu.dk/english/service/phonebook/person?id=26586).

## Files
The RNABC repository contains the following files:

1. RNABC_pipeline.R  
   Main script for the RNABC pipeline including quantile normalization and batch correction for kallisto-derived TPMs for the input data. To replicate the results of this study, the input data is found in the file `Bordet.Rdata` and the CITBCMST data set is found in the file `CITBCMST.Rdata`. 
   The data processing is followed by classification of the Bordet microarray and RNA-seq data using the CITBCMST script found in the `CITBCMST_classifier.R`file.
  
1. CITBCMST_classifier.R  
   Slightly modified version of the original CITBCMST-code found [here](https://CRAN.R-project.org/package=citbcmst). This version generates a more informative plot output by projecting the PCA of the sample data onto the PC space of the CITBCMST training data.

1. Bordet.Rdata  
   Bordet data set saved as an R object. Contains four variables:
   	2. `Bordet_annot` - a dataframe containing information about the 54,675 probe sets on the HG-U133 Plus 2.0 Array.
   	2. `Bordet_array` - a matrix containing microarray expression values for each sample across all 54,675 probe sets.
   	2. `Bordet_RNA_count` - a matrix containing count-level RNA-seq expression values for each sample across all 54,675 probe sets.
   	2. `Bordet_RNA_tpm` - a matrix containing TPM-level RNA-seq expression values for each sample across all 54,675 probe sets.
  
1. CITBCMST.Rdata  
   CITBCMST data set saved as an R object. Contains three variables:
   	3. `CIT_annot` - a dataframe containing information about the 375 probe sets from the HG-U133 Plus 2.0 Array that constitute the signature used for subtyping.
	3. `CIT_core` - a matrix containing microarray expression values for the 355 CITBCSMT core samples across the 375 probe sets.
	3. `CIT_full` - a matrix containing microarray expression values for the 355 CITBCSMT core samples across all 54,675 probe sets.

1. HG-U133_Plus_2_target.fa  
   The HG-U133 Plus 2.0 Target Sequences (originally retrieved from www.affymetrix.com).

1. HG-U133_Plus_2_target.idx  
   kallisto index of the `HG-U133_Plus_2_target.fa` file generated as described in the [kallisto manual](https://pachterlab.github.io/kallisto/manual).

1. CITBCMST_coreset_receptors.txt  
   Tab-delimited file containing receptor status and subtype for the CITBCMST core set (355 samples). The data is derived from [here](https://www.ebi.ac.uk/arrayexpress/experiments/E-MTAB-365/samples/).

The first four files should be downloaded to recreate the results of the study, and the remaining two files are uploaded for easy use in future studies using the presented pipeline. The final file is uploaded for the purpose of batch correction using the receptor status as a co-factor.

*Last updated on August 13, 2018.*